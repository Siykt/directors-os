import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/hs-os/table/list',
    method: 'get',
    params
  })
}
