/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}
export function BNNER_LIST(str) {
  const LIST = [
    {
      img: require('../assets/images/bj0.png'),
      title: '余杭蚁事通',
      content: '一键灭蚁，服务零距离。'
    },
    {
      img: require('../assets/images/bj1.png'),
      title: '余杭蚁事通',
      content: '一键灭蚁，服务零距离。'
    },
    {
      img: require('../assets/images/bj2.png'),
      title: '余杭蚁事通',
      content: '一键灭蚁，服务零距离。'
    },
    {
      img: require('../assets/images/bj3.png'),
      title: '余杭蚁事通',
      content: '一键灭蚁，服务零距离。'
    }
  ]
  return LIST
}
