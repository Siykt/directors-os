import Dialog from '../modules/dialog.vue'
import { mock } from 'mockjs'
export default {
  components: {
    Dialog
  },
  data() {
    return mock({
      showDialogVisible: false,
      isActive: false,
      'BeTakenData|10': [
        {
          RequisitionNumber: 'YH00000@increment',
          'HarmfulPart|1': ['室内', '室外'],
          ContactNumber: /(13|14|15|17|18|19)[0-9]{9}/,
          'State|1': ['已通过', '待审核'],
          ApplicationTime: '@datetime',
          ApplicantName: '@cname'
        }
      ]
    })
  },

  mounted() {},

  methods: {
    seeopen(e) {
      this.isActive = true
    },
    // 操作参数1通过2拒绝3返回
    operation(e) {
      if (e === 1) {
        console.log(e, '通过')
      } else if (e === 2) {
        console.log(e, '拒绝')
      }
      this.isActive = false
    },
    handleClick() {
      this.showDialogVisible = true
    },
    handleClick1() {
      this.$message.success('派遣成功')
    }
  }
}
