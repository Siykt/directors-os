import request from '@/utils/request'

/**
 * 登录
 * @param {number} phoneNumber 手机号
 * @param {string} password 密码
 * @returns 登录token
 */
export function login(phoneNumber, password) {
  return request({
    url: '/hs-os/user/login',
    method: 'post',
    data: {
      phoneNumber,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/hs-os/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/hs-os/user/logout',
    method: 'post'
  })
}

`
<el-table :data="tableData1" border stripe algin-label="center">
<el-table-column
  align="center"
  min-width="150"
  prop="date"
  label="余杭街道"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date1"
  label="仓前街道"
/>
<el-table-column
  align="center"
  prop="date2"
  min-width="150"
  label="闲林街道"
/>
<el-table-column
  align="center"
  prop="date3"
  min-width="150"
  label="五常街道"
/>
<el-table-column
  align="center"
  prop="date4"
  label="中泰街道"
  min-width="150"
/>
</el-table>
<el-table :data="tableData1" border stripe>
<el-table-column
  min-width="150"
  align="center"
  prop="date"
  label="仁和街道"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date1"
  label="良渚街道"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date2"
  label="瓶窑街道"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date3"
  label="径山镇"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date4"
  label="黄湖镇"
/>
</el-table>
<el-table :data="tableData1" border stripe>
<el-table-column
  min-width="150"
  align="center"
  prop="date"
  label="鸬鸟镇"
/>
<el-table-column
  min-width="150"
  align="center"
  prop="date1"
  label="百丈镇"
/>
<el-table-column min-width="150" />
<el-table-column min-width="150" />
<el-table-column min-width="150" />
</el-table>`
